import React from 'react';
import Navbar from '../components/features/navbar/index';
import Container from '../components/shared/container';

export default class About extends React.Component {
  render() {
    return (
      <div>
        <Navbar />
        <Container>
          <h1>About</h1>
        </Container>
      </div>
    );
  }
}
