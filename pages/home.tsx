import React, { useState, useEffect } from 'react';
import Button from '../components/shared/simple-button';
import ArticleListItem from '../components/features/articles/article-list-item/index';
import Navbar from '../components/features/navbar/index';
import Container from '../components/shared/container';
import fetch from 'isomorphic-unfetch';

interface Article {
  id: number;
  title: string;
  url: string;
  points: number;
  user: string;
  time_ago: string;
  comments_count: number;
}

const fetchArticles = (page: number) => {
  return fetch('https://node-hnapi.herokuapp.com/news?page=' + page)
    .then(res => res.json())
    .then((data: Article[]) => data);
};

const Home = props => {
  const [page, setPage] = useState(1);
  const [articles, setArticles] = useState<Article[]>(props.initialArticles);

  useEffect(() => {
    fetchArticles(page).then(setArticles);
  }, [page]);

  let updateArticles = () => {
    setPage(p => p + 1);
  };
  return (
    <div>
      <Navbar />
      <Container>
        {articles.map(article => (
          <ArticleListItem
            id={article.id}
            key={article.id}
            title={article.title}
            url={article.url}
            points={article.points}
            user={article.user}
            time_ago={article.time_ago}
            comments_count={article.comments_count}
          />
        ))}
        <Button onClick={updateArticles}>More</Button>
      </Container>
    </div>
  );
};

Home.getInitialProps = async function() {
  const res = await fetch('https://node-hnapi.herokuapp.com/news?page=1');
  const initialArticles = await res.json();
  return {
    initialArticles,
  };
};

export default Home;
