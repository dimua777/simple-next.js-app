import React from 'react';
import Navbar from '../components/features/navbar/index';

const App = () => (
  <>
    <Navbar />
  </>
);
export default App;
