const path = require('path');

module.exports = {
  webpack(config, options) {
    config.module.rules.push({
      test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
        },
      },
    }),
      (config.resolve.alias['components'] = path.join(__dirname, 'components'));
    return config;
  },
};
