import styled from 'styled-components';

const Container = styled.div`
  width: 84%;
  min-width: 320px;
  max-width: 1700px;
  padding-left: 24px;
  padding-right: 24px;
  box-sizing: border-box;
  outline: none;
  margin: 0 auto;
`;

export default Container;
