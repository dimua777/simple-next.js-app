import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  p {
    margin: 0;
    padding: 0;
  }
  body {
    font-family: Verdana;
  }
`;
