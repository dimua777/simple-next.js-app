import styled from 'styled-components';

const Button = styled.button`
  background: transparent;
  color: #000000;
  margin: 8px;
  padding: 8px 16px;
  font-size: 13px;
`;

export default Button;
