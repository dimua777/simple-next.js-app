import React from 'react';
import Link from 'next/link';
import Navbar from './navbar';
import SiteLogo from './site-logo';
import Container from '../../shared/container';
import GlobalStyle from '../../shared/styles';

export default class Nav extends React.Component {
  render() {
    return (
      <Container>
        <GlobalStyle />
        <Navbar>
          <SiteLogo>sHacker news</SiteLogo>
          <Link href="/home">
            <a>Home</a>
          </Link>
          <Link href="/about">
            <a>About</a>
          </Link>
        </Navbar>
      </Container>
    );
  }
}
