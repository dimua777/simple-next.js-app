import styled from 'styled-components';

const Logo = styled.a`
  font-size: 13px;
  font-weight: 900;
`;

export default Logo;
