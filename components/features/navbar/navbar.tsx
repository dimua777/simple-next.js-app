import styled from 'styled-components';

const Navbar = styled.nav`
  height: 24px;
  background: #ef6537;
  & > a {
    text-decoration: none;
    font-size: 13px;
    padding-left: 16px;
    color: #000000;
  }
`;

export default Navbar;
