import styled from 'styled-components';

const ArticleTitle = styled.a`
  text-decoration: none;
  font-size: 13px;
  color: #000000;
`;

export default ArticleTitle;
