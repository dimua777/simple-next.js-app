import styled from 'styled-components';

const ArticleDescriptionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  & > p {
    color: #828282;
    font-size: 9px;
  }
`;

export default ArticleDescriptionWrapper;
