import styled from 'styled-components';

const ArticleListItemWrapper = styled.div`
  display: flex;
  align-items: center;
  padding-top: 8px;
  background: #f6f6ef;
`;

export default ArticleListItemWrapper;
