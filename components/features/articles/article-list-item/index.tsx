import React, { FC } from 'react';
import ArticleListItemWrapper from './article-list-item-wrapper';
import ArticleId from './article-id';
import ArticleDescriptionWrapper from './article-description-wrapper';
import ArticleTitle from './article-title';

interface ArticleProps {
  id: number;
  title: string;
  url: string;
  points: number;
  user: string;
  time_ago: string;
  comments_count: number;
}

const ArticleListItem: FC<ArticleProps> = props => {
  return (
    <ArticleListItemWrapper>
      <ArticleId>{props.id}.</ArticleId>
      <ArticleDescriptionWrapper>
        <ArticleTitle href={props.url}>{props.title}</ArticleTitle>
        <p>
          {props.points} points by {props.user} {props.time_ago} | hide |{' '}
          {props.comments_count} comments
        </p>
      </ArticleDescriptionWrapper>
    </ArticleListItemWrapper>
  );
};

export default ArticleListItem;
