import styled from 'styled-components';

const ArticleId = styled.p`
  padding: 0px 16px 0px 0px;
  font-size: 13px;
  color: #808080;
`;

export default ArticleId;
